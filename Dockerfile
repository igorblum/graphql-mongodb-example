FROM node:8.10.0-alpine

COPY . /app
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

RUN set -x \
    && yarn install \
    && yarn run build

ENTRYPOINT [ ]
CMD [ "yarn","start" ]