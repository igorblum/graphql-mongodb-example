# GraphQL-MongoDB-Example

```
query Query($pass: JSON!){
  secrets(password: $pass){
    title
    password
  }
}
```

Variables:

```
{
  "pass": "1e61f2f066980ba2b6aa13f47fd6360a"
}
```
